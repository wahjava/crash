# crash

A Very Extensible Shell.

The goal of crash is to be super extensible. The point is to be able
to hack around the shell as much as we want.

Bash and zsh, although powerful, have fairly limited hackability. One
thing I wanted to do was being able to prevent a command from
running, and run something else instead. I couldn't. With crash, I
can.

This is a minimal screenshot of crash in action:

![Crash in action](http://i.imgur.com/q8qhGB9.png)

You can see several things in this screenshot:

- A customized prompt
- A timestamp appended after every command
- A Lisp function call

The customized prompt was defined in the `~/.crashrc`. Look at the
[example crashrc file](example-crashrc) to see how this prompt was
done.

The timestamp is a crash contrib module. The module hooks before every
command is run, and shows a timestamp before running the command.

The Lisp function all is thanks to another crash contrib module. Using
[shelly](https://github.com/fukamachi/shelly) and defining `#` as a
command, whenever you run `# foo`, it's like running `(foo)` in the
Lisp REPL. Even better: it's actually run within crash process!

Crash uses `readline` to manage the prompt. It means that shortcuts
such as `Ctrl-a` (beginning of line), `Ctrl-e` (end of line) or
`Ctrl-r` (history) work out of the box.

### Installation instructions

Requirements: sbcl, make.

```shell
$ git clone https://github.com/ralt/crash
$ cd crash/
$ make
$ sudo make install # the DESTDIR variable is supported.
```

### Contrib modules

Crash provides a list of contrib modules, and some of them are loaded
by default. These modules are:

- `cd`: provides the `cd` command.
- `history`: provides reading history in `~/.history` when crash
  starts, writing to the same file after every command, and the
  command `history`.
- `expansion`: expands the globs such as `~` and `*`.
- `alias`: provides an `alias` command.

Some other contrib modules are provided, and loading them is as simple
as running `(load-module "your-module")` in the `~/.crashrc`.

- `shelly`: provides a `#` command to run shelly commands.
- `time-command`: displays a timestamp after every command.
- `reload`: reloads a contrib module.

### `~/.crashrc`

The `~/.crashrc` file is loaded every time crash is started.

The first line of your crashrc should be `(in-package #:crash-user)`.
This package is provided to avoid conflicts with crash itself.

Here is the definition of the package:

```lisp
(defpackage #:crash-user
  (:use #:cl #:crash))
```

This means that all the symbols exported by crash will be directly
available.


### API

Crash core is very minimal. Even commands such as `cd` are
exported. It provides an API that you can use in your `~/.crashrc` to
hack around whatever you need to.

#### Prompt

To decide what the prompt looks like, crash will call the function
stored in `crash:*prompt-callback*`.

An example

```lisp
(setf *prompt-callback*
      (lambda ()
        (format nil "~A$ " (uiop:getcwd))))
```

Will show a prompt like this:

```shell
/home/foo/bar/$
```

#### Hooks

Crash provides a way to define "hooks". Hooks are called at different
times in crash's life, and they're designed to play together nicely.

There are 2 types of hooks: startup and command.

The startup hooks are run once when crash starts. An example of such a
hook is to load the history.

The command hooks are run every time the user presses `Enter` in the
shell.

Here is an example of such a hook:

```lisp
(defhook example (:weight 10 :type :command) (command arguments next)
  (format t "Hello, world!~%")
  (funcall next command arguments))
```

First off, the `:weight` and `:command` options are optional. The
default values are respectively `0` and `:command`.

The weight defines the order of the hook among all the hooks. The type
is the type of hook (`:command` or `:startup`).

The 3 arguments passed to the hook are:

- `command`: a string with the command that was run. This is the 1st
  item in the command line.
- `arguments`: a list of the arguments passed in the command
  line. Does not include the command.
- `next`: a function that will call the next hook.

The `next` function is unusual. This means that every time your hook
is not concerned by the command that was run, it should call this
function for the next hook to be called. This is what lets crash be
very extensible: the hooks can decide whether they want to stop
everything or not by simply calling or not the next function.

To provide a smoother experience for commands, another macro is
available:

```lisp
(defcommandhook example () (command arguments next)
  (format t "foo~%"))
```

Which expands to something equivalent to this:

```lisp
(defhook example () (command arguments next)
  (unless (string= command "example")
    (return-from example (funcall next command arguments)))
  (format t "foo~%"))
```

#### Modules

To load a module, you simply need `(load-module "your-module")`.

The contrib modules must be in either:

- `/usr/share/crash/contrib/`
- `~/.crash/contrib/`

Contrib modules are plain Lisp systems. You can load them in any way
you like, but crash provides some sugar around this.

For this sugar to work, the modules must follow some convention:

- Every module must be in a folder inside the `contrib/` folder.
- The name of the system must be `crash.<module name>`.
- Hence, the name of the asd file must be `crash.<module name>.asd`.

Crash will load the modules by using quicklisp, so you can put
dependencies in your system, and they will be automatically
downloaded.

The list of default modules is stored in the variable
`*default-contrib-modules*`, and can be changed like any list.
