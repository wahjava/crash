SOURCES := $(wildcard src/*.lisp) $(wildcard *.asd)
QL_LOCAL=$(PWD)/.quicklocal/quicklisp
LOCAL_OPTS=--noinform --noprint --no-sysinit --no-userinit
QL_OPTS=--load $(QL_LOCAL)/setup.lisp
NAME=crash
BINARY_FOLDER=bin

all: $(BINARY_FOLDER)/$(NAME)

deps:
	sbcl $(LOCAL_OPTS) $(QL_OPTS) \
		--eval '(push "$(PWD)/" asdf:*central-registry*)' \
		--eval '(ql:quickload :$(NAME))' \
		--eval '(quit)'
	touch $@

$(BINARY_FOLDER):
	mkdir -p $(BINARY_FOLDER)

$(BINARY_FOLDER)/$(NAME): $(SOURCES) $(QL_LOCAL)/setup.lisp deps $(BINARY_FOLDER)
	sbcl $(LOCAL_OPTS) $(QL_OPTS) \
		--eval '(push "$(PWD)/" asdf:*central-registry*)' \
		--eval "(asdf:operate 'asdf:build-op :$(NAME))" \
		--eval '(quit)'

.PHONY: clean install

install: $(BINARY_FOLDER)/$(NAME)
	mkdir -p $(DESTDIR)/usr/bin
	cp $(BINARY_FOLDER)/$(NAME) $(DESTDIR)/usr/bin/$(NAME)
	mkdir -p $(DESTDIR)/usr/share/crash/
	cp -R contrib/ $(DESTDIR)/usr/share/crash/

clean:
	rm -rf deps .quicklocal $(BINARY_FOLDER) quicklisp.lisp

$(QL_LOCAL)/setup.lisp: quicklisp.lisp
	sbcl --noinform --noprint --disable-debugger --no-sysinit --no-userinit \
		--load quicklisp.lisp \
		--eval '(quicklisp-quickstart:install :path "$(QL_LOCAL)")' \
		--eval '(quit)'

quicklisp.lisp:
	wget https://beta.quicklisp.org/quicklisp.lisp
	echo '4a7a5c2aebe0716417047854267397e24a44d0cce096127411e9ce9ccfeb2c17 *quicklisp.lisp' | shasum -c -
