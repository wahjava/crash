(in-package #:crash)

(defun default-prompt-callback ()
  (format nil
          "[~A@~A ~A]~A "
          (slot-value (sb-posix:getpwuid (sb-posix:getuid)) 'sb-posix::name)
          (let ((hostname (uiop:read-file-string "/etc/hostname")))
            (subseq hostname 0 (1- (length hostname))))
          (first (last (pathname-directory (uiop:getcwd))))
          (if (= (sb-posix:getuid) 0) "#" "$")))

(export '*prompt-callback*)
(defvar *prompt-callback* #'default-prompt-callback
  "To display the prompt, the function stored in this variable
is used. It must return a string.")
