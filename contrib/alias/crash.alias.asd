(defpackage #:crash.alias
  (:use #:cl #:crash))

(in-package #:crash.alias)

(asdf:defsystem #:crash.alias
  :description "Alias plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "alias")))
