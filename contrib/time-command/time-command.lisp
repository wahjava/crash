(in-package #:crash.time-command)

(defhook time-command (:weight -500) (command arguments next)
  (multiple-value-bind (seconds minutes hours)
      (get-decoded-time)
    (format t "[~2,'0D:~2,'0D:~2,'0D]~%" hours minutes seconds))
  (funcall next command arguments))
