(defpackage #:crash.time-command
  (:use #:cl #:crash))

(in-package #:crash.time-command)

(asdf:defsystem #:crash.time-command
  :description "Time-Command plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "time-command")))
