(in-package #:crash.cd)

(defcommandhook cd () (command arguments next)
  (when (> (length arguments) 1)
    (error "Too many arguments to cd."))
  (let ((oldpwd (namestring (uiop:getcwd))))
    (uiop:chdir
     (cond
       ((= (length arguments) 0) (user-homedir-pathname))
       ((string= (first arguments) "-") (if (uiop:getenv "OLDPWD")
                                            (uiop:getenv "OLDPWD")
                                            ;; Don't move if we don't have OLDPWD
                                            (oldpwd)))
       (t (first arguments))))
    (setf (uiop:getenv "OLDPWD") oldpwd)))
