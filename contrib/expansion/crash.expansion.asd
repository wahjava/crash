(defpackage #:crash.expansion
  (:use #:cl #:crash))

(in-package #:crash.expansion)

(asdf:defsystem #:crash.expansion
  :description "Expansion plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash :cl-ppcre)
  :components ((:file "expansion")))
