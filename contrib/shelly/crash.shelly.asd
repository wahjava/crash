(defpackage #:crash.shelly
  (:use #:cl #:crash))

(in-package #:crash.shelly)

(asdf:defsystem #:crash.shelly
  :description "Shelly plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash :shelly)
  :components ((:file "shelly")))
